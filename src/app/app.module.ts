import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PainelGestaoComponent } from './pages/painel-gestao/painel-gestao.component';
import { ContaDigitalComponent } from './pages/conta-digital/conta-digital.component';
import { AntRecebiveisComponent } from './pages/ant-recebiveis/ant-recebiveis.component';
import { HomeComponent } from './pages/home/home.component';
import { RouterModule } from '@angular/router';
import { ROUTES } from './app.route'

@NgModule({
  declarations: [
    AppComponent,
    PainelGestaoComponent,
    ContaDigitalComponent,
    AntRecebiveisComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
