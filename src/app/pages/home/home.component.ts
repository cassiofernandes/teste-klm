import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  aba: string = 'home';

  @Input() contador: any = '';

  imgLogo:any = '/assets/imagens/logo.png';

  constructor() { }

  ngOnInit() {
  }

}
