import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-conta-digital',
  templateUrl: './conta-digital.component.html',
  styleUrls: ['./conta-digital.component.css']
})
export class ContaDigitalComponent implements OnInit {

  painel: string = 'Painel de Gestão';

  listaAlertas:any[] = 
        [{
            aviso:'Notas Canceladas',
            controle: 'ocultar',
            contador: 23
        },
        {
            aviso:'Notas Canceladas',
            controle: 'exibir',
            contador: 13
        },
        {
            aviso:'Assinaturas Digitais',
            controle: 'exibir',
            contador: 7
        }   
      ]
      
  constructor() { }

  ngOnInit() {
  }

}
