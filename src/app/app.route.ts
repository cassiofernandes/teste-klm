import { Routes, ActivatedRoute } from '@angular/router'
import { ContaDigitalComponent } from './pages/conta-digital/conta-digital.component'
import { AntRecebiveisComponent } from './pages/ant-recebiveis/ant-recebiveis.component'
import  { PainelGestaoComponent } from './pages/painel-gestao/painel-gestao.component'

export const ROUTES : Routes = [
    {path: '', component: ContaDigitalComponent},
    {path: 'AntRecebiveis', component: AntRecebiveisComponent},
    {path: 'Gestao', component: PainelGestaoComponent}
   

]